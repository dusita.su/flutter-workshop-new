import 'package:flutter/material.dart';

void main() {
  var app = MaterialApp(
    title: "My App",
    home: Scaffold(
      appBar: AppBar(
        title: Text("My first App"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image(
            image: NetworkImage(
                "https://ae01.alicdn.com/kf/H799cdbc0a39b4110a5231ee021814749s/Mascot-Bullfrog.jpg_640x640.jpg"),
            width: 350,
            height: 200,
          ),
          // Text("hello"),
          // Text("wellcome"),
          // Text("have room")
          Text(
            'Hello my App opop', // ข้อความที่ต้องการแสดง
            style: TextStyle(
              fontSize: 20, // กำหนดขนาดตัวอักษร
              fontWeight: FontWeight
                  .bold, // กำหนดความหนาของตัวอักษร // กำหนดลักษณะตัวอักษร (ตัวเอียง)
              letterSpacing: 1.5, // กำหนดระยะห่างระหว่างตัวอักษร
              wordSpacing: 2.0, // กำหนดระยะห่างระหว่างคำ
              color: Colors.green, // กำหนดสีของตัวอักษร
              // สามารถกำหนดคุณสมบัติอื่น ๆ ตามความต้องการ
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16.0),
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                    labelText: 'op op',
                    border: OutlineInputBorder(),
                  ),
                  onChanged: (value) {
                    print('ข้อความที่ป้อน: $value');
                  },
                ),
                SizedBox(
                    height: 16.0), // กำหนดระยะห่างในแนวดิ่งระหว่าง TextField
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: 'op op op',
                    border: OutlineInputBorder(),
                  ),
                  onChanged: (value) {
                    print('ข้อความที่ป้อน: $value');
                  },
                ),
              ],
            ),
          ),
          SizedBox(height: 16.0),
          ElevatedButton(
            onPressed: () {
              print('login');

              // สามารถเพิ่มโค้ดหรือฟังก์ชันที่ต้องการทำงานเมื่อปุ่มถูกคลิก
            },
            child: Text('login op op'),
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
              textStyle: TextStyle(fontSize: 18.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ),
        ],
      )),
    ),
    theme: ThemeData(primarySwatch: Colors.green),
  );
  runApp(app);
}
